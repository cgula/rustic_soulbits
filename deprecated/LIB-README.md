# Soulbits Library

## Design

### Library Crate

![img](/assets/imgs/Program-Structural-Diagram.jpg)

***Figure 1.** The basic structure of the room as the interface with the objects and each of their properties they hold. ```'...'``` denotes that more objects can be added.*

The structure consists of the basic overarching design of the program interface foundation.

+ Room: Is the environment state for which the user finds themselves in.
+ Each of the objects cooresponds to an interactive piece found in the room. Currentlt this includes
  + Dusty book
  + Cracked Television
  + ~~Old~~ Staticky Radio
  + ~~Pile of Junk~~ Creaky Treasure Chest
  + [Optional] Mimic
+ Each item will have a collection of attributes where
  + Each attribute holds a certain number of items in a list
  + Ex: A channel on the television will hold 5 favorite movies
+ Each item will have a description
+ [Optional] Each item will include more complex assets such as:
  + An animation
  + A sound that pertains to the item
  + A graphic

## <!--library-line--> <!-- omit in toc -->

## Testing

![img](/assets/imgs/test-reader.png)
***Figure 2.** Testing for reading in text from a file.*
