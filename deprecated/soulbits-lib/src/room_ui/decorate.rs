// Should this be in room-or somewhere else?
pub(crate) struct FurtnitureFixtures {
    // Type Book
    bookcase: Vec<String>,
    // Type Channel
    cracked_tv: Vec<String>,
    // Type Station
    staticky_radio: Vec<String>,
    // Type DustyItem
    creaky_treasure_chest: Vec<String>,
}

trait Layout {

}

impl Layout for FurtnitureFixtures{

}