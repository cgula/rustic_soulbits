// Finishing Materials for a default room

enum FinishingMaterial {
    Door,
    Wall,
    Floor,
    Ceiling,
}

// Create the room format
struct Blueprint {
    setup: Vec<FinishingMaterial>,
}

// Where containers go
impl Blueprint {
    // pub(crate) fn build_room

    fn draw_finishing(&self) {}

}