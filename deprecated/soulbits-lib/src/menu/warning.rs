use text_box::TextBox;
use text_box::utils::clear_screen;

const MSG: [&str; 2] = ["+-- CONTENT WARNING --+",
"This project may contain sensitive content that some
people may find disturbing such as \n
+-- themes of horror,
\n +-- explicit language, and
\n +-- mental health references."];
    
    
#[derive(Copy, Clone)]
struct Warning <T, M> {
    title: T,
    msg: M,
}
    
    
impl <T, M> Warning <T, M> {
    fn to_display(self) -> (T, M) {
        (self.title, self.msg)
    }
    
    //fn display_msg

    pub(crate) fn warning(&self, mut warned: bool) -> bool{
        clear_screen();

        let warn_type = Warning{title: MSG[0], msg: MSG[1]};
        let output = warn_type.to_display();

        let warning = TextBox::new(
            3, 1,
            15, 6,
            2,
            output.0,
            output.1,
        ).unwrap();

        if !warned {
            println!("{}", warning);
            warned = true;
        }

        warned
    }
}
    
//pub fn display() {
//    let mut warned = false;
//    crate::warning(warned);
//}