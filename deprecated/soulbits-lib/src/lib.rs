//! Soulbits Library
//! Rust Programming - Bart Massey
//! Cordet Gula Fall 2023

// This crate is a library
#![crate_type = "lib"]

mod characters;
mod items;
mod menu;
//mod room_ui;
mod storyline;

// Load from files
mod reader{
    use std::fs::read_to_string;

    const FILENAMES: [&str;2] = ["src/.menu-msg.txt", "src/.storyline.txt"];

    // Rust by example: naive
    fn to_parse(a_file: &str) -> Vec<String> { 
        read_to_string(a_file)
        .unwrap()
        .lines()
        .map(String::from)
        .collect()
    }

    fn storylines() {
       let test = "hi"; 
        println!("{}", test);
    }

    // ----------------------------
    // Unit test module for reader
    #[cfg(test)]
    mod tests {
        use super::*;
        #[test]
        fn test_reader() {
            let file: &str = FILENAMES[1];
            let test_vec: Vec<String> = to_parse(file);
            assert_eq!("\"When you look into an abyss, the abyss also looks into you.\" - Friedrich Nietzsche", test_vec[0]);
        }
    }
}
