# ~ Rustic SoulBits ~ <!-- omit in toc -->

<!-- Table of Contents Dropdown-->
<details><summary><h2>Table of Contents</h2></summary>

1. [About Rustic SoulBits](#about-rustic-soulbits)
2. [Topic  Area](#topic--area)
3. [Vision](#vision)
4. [Concerns](#concerns)
   1. [Challenges](#challenges)
5. [Design](#design)
   1. [Interface](#interface)
   2. [Story](#story)
   3. [Assets](#assets)
      1. [Audio](#audio)
      2. [ASCII Art](#ascii-art)
      3. [Graphics](#graphics)
      4. [Animations](#animations)
6. [Dependencies](#dependencies)
7. [packages used](#packages-used)
8. [Add your files](#add-your-files)
9. [Test and Deploy](#test-and-deploy)
10. [Installation](#installation)
11. [Usage](#usage)
12. [Support](#support)
13. [Roadmap](#roadmap)
14. [Authors and acknowledgment](#authors-and-acknowledgment)
15. [License](#license)
16. [Project status](#project-status)
17. [Final Thoughts](#final-thoughts)
18. [References](#references)
  
</details>

<!-- End Table of Contents -->
## <!-- omit in toc -->

<!-- Show tree -->
<!-- 1. How to play/use program -->
<!-- 2. Development instructions -->
<!-- 3. Author Notes -->

## About Rustic SoulBits

Carpe Noctem

## Topic  Area

Carpe Noctem

## Vision

Carpe Noctem

## Concerns

**Beginning Concerns:**
Change concerns to challenges

## <!--concerns-update-line--> <!-- omit in toc -->

### Challenges

#### Graphics <!-- omit in toc -->

Discuss future of graphics

#### modules, traits, and objects, oh my! <!-- omit in toc -->

Another challenge I had when figuring out the design was how to structure everything. Rust does OOP a bit different than I'm used to, without classes or using hierarchies. I spent some time going over the different uses of each. I think the more confusing concept to wrap my mind around was traits and modules. Structs and enums I understand just fine, but traits were used in
a couple different ways. In some examples, traits are used similar to a class and a struct was used as a specific instance of the trait.

```rust
// https://doc.rust-lang.org/stable/rust-by-example/trait/dyn.html

struct Sheep {}

trait Animal {
  // methods
}

impl Animal for Sheep {
  // implement methods
}
```

In other examples, a trait is a property applied to an object.

```rust
// https://doc.rust-lang.org/book/ch17-03-oo-design-patterns.html

trait State {
  // methods
}

struct Draft {}

impl State for Draft {
  // implementation of methods
}
```

While a module is described as a collection of items that can have public and private visibility. In this way, traits and modules seem to have been created from splitting the
functionality of classes.

So, for example, when creating the structure for a Room, I was unsure of whether a Room should be a module, a trait, or an object.

My first thought was have a Room as a trait:

```rust
trait Room {}
trait Tower: Room {}

enum StartRoom {}

impl Room for StartRoom {}
impl Tower for StartRoom {}
```

~~But pondering this a bit more, I thought a better approach would be to create a Room module
with a State trait, an Empty: State trait, a Start: State trait, and a RoomType enum + a Layout trait and a containers struct. I came to this decision because I wondered if it would be useful
to have some properties be private &rarr; Yes.~~

Modules are used to seperate code within a library -- a library consists of multiple modules.

## Design

See LIB and BIN README

### Interface

#### Start <!-- omit in toc -->

+ [ ] A greeting
+ [ ] An introduction
+ [ ] Rules/Expectations including how to quit [i.e., a menu]
+ [ ] [optional] an opening scene
+ [ ] 1. The begining structure is going to operate like a basic text-adventure game.
  + [ ] 2. Updgrade to Ascii art
  + [ ] 3. Upgrade to 8bit graphics

## <!--interface-line--> <!-- omit in toc -->

### Story

+ [ ] User wakes up in weird castle room
  + Yup, you guessed it -- with no idea of how they got there -- No need for the ```How cliché``` :roll_eyes: eyeroll
+ [ ] The user ...
  + [ ] is greeted by a stranger ***OR***
  + [ ] the user is alone and thinks to themselves *Where ~~the fuck~~ am I?*

> :memo: **NOTE:** More depth to be developed later.

## <!--story-line--> <!-- omit in toc -->

### Assets

> :memo: **NOTE-TO-SELF:** Assets are lowest priority and are ***optional*** additions at this point in development.

#### Audio

+ [ ] background music
+ [ ] sound FX

#### ASCII Art

It would be nice to start with some visuals so ASCII Art then &rarr; 8bit graphics.

#### Graphics

The goal is create my own graphics using pixcel. I already designed a wall texture:

![Gif of Wall Texture Art Process](/assets/imgs/picxel-wall-texture.gif)

#### Animations

## <!--assets-line--> <!-- omit in toc -->

## Dependencies

```text
Linux - gtk
druid
window
```

## packages used

```text
pixcil
```

## Add your files

## Test and Deploy

***

## Installation

Something

## Usage

Visuals/Examples

***

## Support

...

## Roadmap

In the future I have a vision for Rustic Soulbits to integrate more attributes revolving around shadow work and analysis of the self. Many horror genre games and movies depict underlying psychological themes around the human experience and themselves. Some of these themes include body-horror or repressed personality traits.

My hope is to include more levels to the haunted mansion that shows more themes of self-discovery related to a specific protagonist. I personally enjoy how Silent Hill has monsters related to the protagonist's psyche of repressed personality traits, how Bloodborne represents horror themes around femininity, and Dark Souls integrates the experience of depression and despair.

So my hopes for Rustic Soulbits is to develop into a functional game with a deep storyline about people's experiences that are not well understood and thus, being a therapeutic vice to help those parts feel validated and understood -- or for people just to enjoy another psychological horror game.

## Authors and acknowledgment

...

## License

[MIT LICENSE](https://gitlab.cecs.pdx.edu/cgula/rustic_soulbits/LICENSE)

## Project status

WIP

***

## Final Thoughts

Put thoughts about overarching experience of rust, the project, etc.

***

## References

<!-- References to Others' Contribution -->
NTS: Don't forget to add reference file

<!-- Reference link list for README -->
NTS: Add link references to clean up README

<!-- FootNote References -->
NTS: For in-line references
