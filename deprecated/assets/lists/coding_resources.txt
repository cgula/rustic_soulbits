Use text-to-speech to talk though your code with this virtual rubber duck! Now you do not have to worry about obtaining a rubber duck through theft! {https://spiggiecode.github.io/rubberduck/}
